
import tensorflow as tf
AUTOTUNE = tf.data.experimental.AUTOTUNE


class Data:
    def __init__(self, dir_name, validation_split=0.2, batch_size=32, img_height=200, img_width=200):
        self.dir_name = dir_name
        self.batch_size = batch_size
        self.img_height = img_height
        self.img_width = img_width
        self.validation_split = validation_split

    def get_images(self, type):
        train_ds = tf.keras.preprocessing.image_dataset_from_directory(
            self.dir_name,
            validation_split=self.validation_split,
            subset=type,
            seed=123,
            image_size=(self.img_height, self.img_width),
            batch_size=self.batch_size)
        return train_ds

    def load_data(self):
        train_ds = self.get_images("training")
        val_ds = self.get_images("validation")
        return train_ds, val_ds
