from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf


new_model = tf.keras.models.load_model('../models/new_model')

new_model.summary()