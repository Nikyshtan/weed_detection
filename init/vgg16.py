from __future__ import absolute_import, division, print_function, unicode_literals

from keras_preprocessing.image import ImageDataGenerator

from classes.Data import Data
import matplotlib.pyplot as plt
from tensorflow.keras import layers, models
import numpy as np
import tensorflow as tf
import os
import datetime


dir_name = "../images/images"

img_height = 400
img_width = 400
batch_size = 8
epochs = 40

info = Data(dir_name, img_height=img_height, img_width=img_width)
(train_images, test_images) = info.load_data()
classNames = train_images.class_names
#print(classNames)
#
#for image_batch, labels_batch in train_images:
#    print(image_batch.shape)
#    print(labels_batch.shape)
#    break

plt.figure(figsize=(10, 10))
for images, labels in train_images.take(1):
    for i in range(9):
        ax = plt.subplot(3, 3, i + 1)
        plt.imshow(images[i].numpy().astype("uint8"))
        plt.title(classNames[labels[i]])
        plt.axis("off")
plt.show()

AUTOTUNE = tf.data.experimental.AUTOTUNE
train_ds = train_images.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
val_ds = test_images.cache().prefetch(buffer_size=AUTOTUNE)


#START CREACION DEL MODELO#

num_classes = len(classNames)

model = models.Sequential()

#Data augmentation
model.add(layers.experimental.preprocessing.RandomFlip("horizontal",
                                                 input_shape=(img_height,
                                                              img_width,
                                                              3)))
model.add(layers.experimental.preprocessing.RandomRotation(0.1))
model.add(layers.experimental.preprocessing.RandomZoom(0.1))


model.add(layers.experimental.preprocessing.Rescaling(1./255, input_shape=(img_height, img_width, 3))) #3 por RGB
model.add(layers.Conv2D(32,3, padding='valid', activation='relu'))
model.add(layers.Conv2D(32,3, padding='valid', activation='relu'))
model.add(layers.MaxPooling2D())

model.add(layers.Conv2D(64, 3, padding='valid', activation='relu'))
model.add(layers.Conv2D(64, 3, padding='valid', activation='relu'))
model.add(layers.MaxPooling2D())

model.add(layers.Conv2D(128, 3, padding='valid', activation='relu'))
model.add(layers.Conv2D(128, 3, padding='valid', activation='relu'))
#model.add(layers.Conv2D(128, 3, padding='valid', activation='relu'))
model.add(layers.MaxPooling2D())

model.add(layers.Flatten())
model.add(layers.Dense(256, activation='relu'))
model.add(layers.Dropout(0.35))
model.add(layers.Dense(num_classes, activation='softmax'))

model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

model.summary()
#END CREACION DEL MODELO#
datetime = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
log_dir = "logs/vgg16" + datetime
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
model_callback = tf.keras.callbacks.ModelCheckpoint('../models/best_models/modelcallback'+datetime+'.h5',monitor='val_loss', verbose=1, save_best_only=False, save_weights_only=False, mode='auto', period=1)

history = model.fit(
    train_images,
    validation_data=test_images,
    epochs=epochs,
    callbacks=[tensorboard_callback, model_callback]
)

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss=history.history['loss']
val_loss=history.history['val_loss']

epochs_range = range(epochs)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Exito entrenamiento')
plt.plot(epochs_range, val_acc, label='Exito test')
plt.legend(loc='lower right')
plt.title('Exito de entrenamiento y test')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Perdida entrenamiento')
plt.plot(epochs_range, val_loss, label='Perdida test')
plt.legend(loc='upper right')
plt.title('Perdida de entrenamiento y test')
plt.show()
model.save('../models/vgg16'+datetime)

print("Modelo guardado")



