from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np
import os
import tensorflow as tf

from tensorflow import keras
import matplotlib.pyplot as plt

def all_images(directory):
    names_array=[]
    for image in os.listdir(directory):
        image_dir=directory+'/'+image.title()
        img = keras.preprocessing.image.load_img(
            image_dir, target_size=(400, 400)
        )
        img_array = keras.preprocessing.image.img_to_array(img)
        img_array = tf.expand_dims(img_array, 0)  # Create a batch

        predictions = model.predict(img_array)
        score = tf.nn.softmax(predictions[0])

        plt.figure(figsize=(5, 5))
        plt.imshow(img)
        plt.title("{} {:.2f} %".format(class_names[np.argmax(score)]+' ('+image.title()+')', 100 * np.max(score)))
        names_array.append(class_names[np.argmax(score)])

        count = 0
        y = 420
        for class_name in class_names:
            plt.text(s="{} {:.2f} %".format(class_name, 100 * score[count]), x=0, y=y)
            count += 1
            y += 15

        plt.axis("off")
        plt.show()
    return names_array


#model = tf.keras.models.load_model('../models/20201118-194819') 20 iteraciones
#model = tf.keras.models.load_model('../models/20201118-182841') #Dio buenos resultados
#model = tf.keras.models.load_model('../models/20210412-181319') #Ultimo con dropout
model = tf.keras.models.load_model('../models/best_models/modelcallback20210414-001238.h5') #Ultimo con dropout, bueno con la rama negra
#model = tf.keras.models.load_model('../models/best_models/modelcallback20210414-141229.h5') #Ultimo con dropout
#model = tf.keras.models.load_model('../models/best_models/modelcallback20210414-164838.h5') #Arquitectura Vgg16


model.summary()
class_names = ['Rama Negra', 'Roseta', 'Yuyo Colorado']

image_dir = '../images/test/ramanegra3.jpg'
directory = '../images/test/yuyo colorado'
labels=all_images(directory)
print(labels)
# img = keras.preprocessing.image.load_img(
#     image_dir, target_size=(400,400)
# )
# img_array = keras.preprocessing.image.img_to_array(img)
# img_array = tf.expand_dims(img_array, 0) # Create a batch
#
# predictions = model.predict(img_array)
# score = tf.nn.softmax(predictions[0])
#
# plt.figure(figsize=(5, 5))
# plt.imshow(img)
# plt.title("{} {:.2f} %".format(class_names[np.argmax(score)], 100 * np.max(score)))
# count = 0
# y=420
# for class_name in class_names:
#     plt.text(s="{} {:.2f} %".format(class_name, 100 * score[count]), x=0,y= y)
#     count += 1
#     y +=15
#
# plt.axis("off")
# plt.show()



